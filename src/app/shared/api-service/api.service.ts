import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  constructor(private http: HttpClient) {}
  currentUser: any = {};
  tokenUser: string = '';
  changePassword() {
    return this.http.post(
      `${environment.urlAuth0API}/dbconnections/change_password`,
      {
        client_id: environment.clientId,
        email: this.currentUser.email,
        connection: 'Username-Password-Authentication',
      },
      {
        headers: {
          'content-type': 'application/json',
        },
      }
    );
  }
  getServerAPI(url: string): Observable<any> {
    return this.http.get(`${environment.urlServerAPI}/${url}`, {
      headers: { Authorization: `Bearer ${this.tokenUser}` },
    });
  }

  postServerAPI(url: string, data: string) {
    return this.http.post(
      `${environment.urlServerAPI}/${url}`,
      { Messages: data },
      {
        headers: { Authorization: `Bearer ${this.tokenUser}` },
      }
    );
  }
}
