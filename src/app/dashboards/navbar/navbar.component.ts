import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '@auth0/auth0-angular';
import { APIService } from 'src/app/shared/api-service/api.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  selectedOption = '1';
  user: any;
  foods: Food[] = [
    {value: '1', viewValue: 'User Management'},
    {value: '2', viewValue: 'DSP Accessibility'},
    {value: '3', viewValue: 'League Accessibility'},
  ];
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private authService: AuthService,
    public dialog: MatDialog,
    public apiService: APIService
  ) {
    this.user = {};
  }
  openDialog() {
    this.dialog.open(ChangePasswordComponent);
  }
  ngOnInit(): void {
    this.authService.user$.subscribe((success: any) => {
      this.user = success;
      this.apiService.currentUser = success;
    });
  }

  changePassword(): void {
    this.apiService.changePassword().subscribe(
      () => {
        this.dialog.open(ChangePasswordComponent);
      },
      () => {
        this.dialog.open(ChangePasswordComponent);
      }
    );
  }

  signOut(): void {
    this.authService.logout({
      returnTo: this.document.location.origin,
    });
  }
}
