export interface IDataHistory {
  data: IItemHistory[];
  message: string;
  metadata: IMetadataHistory;
}
export interface IItemHistory {
  role: string;
  authorizeBy: string;
  status: string;
  id: number;
  name: string;
  email: string;
}
export interface IMetadataHistory {
  total: number;
}
export interface IFormatData {
  date: string;
  time: string;
  downloadRecordUrl: string;
  streamUrl: string;
  call_sid: string;
  created_at: string;
  duration: number;
  id: number;
  phone: string;
  record_url: string;
}
