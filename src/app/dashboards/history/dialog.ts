import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
export interface DialogData {
    animal: string;
    name: string;
}

@Component({
    selector: 'dialog-overview-example-dialog',
    template: `<div class="dialog-custom">
        <h1 mat-dialog-title>Alert Notification</h1>
    <p class="close-button" (click)="onNoClick()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
  <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
</svg></p>
    <div mat-dialog-content>
      <p>If you take this action, this user will be {{data.name}} permanently. Are you sure?</p>
    </div>
    <div mat-dialog-actions class="delete-nav">
      <button mat-raised-button (click)="onNoClick()">Cancel</button>
      <button mat-raised-button [color]="data.name == 'locked' ? 'primary' : 'warn'">{{data.name == 'locked' ? 'Lock' : 'Delete'}}</button>
    </div>
    </div>`,
    styles: ['.dialog-custom{position:relative} .delete-nav { display: flex; justify-content:space-between} .lock {background-color: red} .close-button{position:absolute; top: -13px; right: -10px; }']

})
export class DialogOverviewExampleDialog {
    constructor(
        public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}