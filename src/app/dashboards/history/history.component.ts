import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';

import {
  MatPaginator,
  MatPaginatorIntl,
  PageEvent
} from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DialogOverviewExampleDialog } from './dialog';
import { HistoryService } from './history.service';
import { IItemHistory } from './type';

const MY_FORMATS = {
  parse: { dateInput: 'YYYY/MM/DD' },
  display: {
    dateInput: 'YYYY/MM/DD',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'YYYY/MM/DD',
    monthYearA11yLabel: 'MM YYYY',
  },
};
const japaneseRangeLabel = (page: number, pageSize: number, length: number) => {
  return `${page + 1}/${Math.ceil(length / pageSize)} page`;
};

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_LOCALE, useValue: 'ja-JP' },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MatPaginatorIntl, useValue: getDutchPaginatorIntl() },
  ],
})
export class HistoryComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  filterForm!: FormGroup;
  constructor(private service: HistoryService, private fb: FormBuilder, public dialog: MatDialog) {
    this.sortedData = this.temp.slice();
  }
  isLoading = false;
  length = 0;
  pageSize = 4;
  pageIndex = 1;
  pageSizeOptions: number[] = [4, 8, 12, 16];
  pipe = new DatePipe('en-US');
  urlDowload = `call-histories/csv?page=${this.pageIndex}&limit=${this.pageSize}`;
  displayedColumns: string[] = [
    'position',
    'number',
    'date',
    'duration',
    'records',
    'download',
  ];
  dataSource = new MatTableDataSource<PeriodicElement>([]);
  rawData: IItemHistory[] = [];
  handlePageEvent(event: PageEvent) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.reload();
  }

  ngOnInit(): void {
    this.filterForm = this.fb.group({
      searchInput: new FormControl(''),
      pickerStart: new FormControl(''),
      pickerEnd: new FormControl(''),
    });
    this.reload();
    this.filterForm.valueChanges
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe(() => {

        
        this.pageIndex = 1;
        this.reload(this.filterForm.value.searchInput);
      });
  }
  sortedData:any;
  public temp:any = [
    {
      id: 1,
      name: 'User 1',
      email: 'user1@gmail.com',
      authorizeBy: 'Parent A',
      status: 'Locked'
    },
    {
      id: 2,
      name: 'User 2',
      email: 'user2@gmail.com',
      authorizeBy: 'Parent A',
      status: 'Active'
    },
    {
      id: 3,
      name: 'User 3',
      email: 'user3@gmail.com',
      authorizeBy: 'Dealer parent 1',
      status: 'Active'
    },
    {
      id: 4,
      name: 'User 4',
      email: 'user4@gmail.com',
      authorizeBy: 'Parent A',
      status: 'Locked'
    },
    {
      id: 5,
      name: 'User 5',
      email: 'user5@gmail.com',
      authorizeBy: 'Dealer parent 1',
      status: 'Active'
    }
  ]

  reload(search?: string) {
    this.refreshUrlDownload();
    this.dataSource = new MatTableDataSource();
    if( search === undefined){
      this.dataSource = this.sortedData;
    }
    else{
      this.dataSource = this.sortedData.filter((d:any)=>d.name.toLowerCase().includes(search.toLowerCase()))
    }
    
  }

  refreshUrlDownload() {
    this.urlDowload = `call-histories/csv?page=${this.pageIndex}&limit=${
      this.pageSize
    }&phone=${this.filterForm.get('searchInput')?.value}&from=${
      this.filterForm.get('pickerStart')?.value
    }&to=${this.filterForm.get('pickerEnd')?.value}`;
  }

  dowloadFilter() {
    this.service.downloadFilterRecord(this.urlDowload).subscribe(
      (data) => {
        var jsonObject = data;
        this.exportCSVFile(jsonObject);
      },
      (err) => {
        var jsonObject = err.error.text;
        this.exportCSVFile(jsonObject);
      }
    );
  }

  exportCSVFile(items: string) {
    var exportedFilenmae = 'export.csv';
    var blob = new Blob([items], { type: 'text/csv;charset=utf-8;' });
    var link = document.createElement('a');
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilenmae);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  openDialog(name: string): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      data: {name: name},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  sortData(sort: Sort) {
    const data = this.temp.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }
    this.sortedData = data.sort((a:any, b:any) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return this.compare(a.name, b.name, isAsc);
        case 'id':
          return this.compare(a.id, b.id, isAsc);
        default:
          return 0;
      }
    });
    this.reload();
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}

export interface PeriodicElement {
  phone: string;
  date: string;
  time: string;
  duration: number;
  record_url: string;
  call_sid: string;
  id: number;
  download?: string;
}

export function getDutchPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Items per page:';
  paginatorIntl.nextPageLabel = '前へ';
  paginatorIntl.firstPageLabel = '初ページ';
  paginatorIntl.lastPageLabel = '終ページ';
  paginatorIntl.previousPageLabel = '次へ';
  paginatorIntl.getRangeLabel = japaneseRangeLabel;

  return paginatorIntl;
}