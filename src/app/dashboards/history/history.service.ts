import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIService } from 'src/app/shared/api-service/api.service';
import { IDataHistory } from './type';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  constructor(private apiService: APIService) {}
  getCallHistory(
    page: number,
    limit: number,
    phone: string,
    from: string,
    to: string
  ): Observable<IDataHistory> {
    return this.apiService.getServerAPI(
      `call-histories?page=${page}&limit=${limit}&phone=${phone}&from=${from}&to=${to}`
    );
  }
  downloadCallList(
    page: number,
    limit: number,
    phone: string,
    from: string,
    to: string
  ): Observable<any> {
    return this.apiService.getServerAPI(
      `call-histories/csv?page=${page}&limit=${limit}&phone=${phone}&from=${from}&to=${to}`
    );
  }
  downloadRecord(id: number): Observable<any> {
    return this.apiService.getServerAPI(`call-histories/${id}/download-record`);
  }
  downloadFilterRecord(url: string): Observable<any> {
    return this.apiService.getServerAPI(url);
  }
}
