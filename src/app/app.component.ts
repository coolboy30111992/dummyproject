import { DOCUMENT } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { APIService } from './shared/api-service/api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'helene';
  isAuthenticated: boolean;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private authService: AuthService,
    private apiService: APIService
  ) {
    this.isAuthenticated = false;
  }

  public ngOnInit(): void {
    this.authService.isAuthenticated$.subscribe((success: boolean) => {
      this.isAuthenticated = success;
    });
    this.authService.idTokenClaims$.subscribe((success: any) => {
      this.apiService.tokenUser = success.__raw;
    });
  }

  public signOut(): void {
    this.authService.logout({
      returnTo: this.document.location.origin,
    });
  }
}
