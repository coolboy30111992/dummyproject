// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  domain: 'prd-helene-call-monitoring.jp.auth0.com',
  urlAuth0API: 'https://prd-helene-call-monitoring.jp.auth0.com',
  urlServerAPI:
    'https://one3jec4xb.execute-api.ap-northeast-1.amazonaws.com/dev',

  //Currently Infor
  clientId: 'MmvC7HUTl85efwhiVs1i4HsdRs7lV1Ej',
  urlSocket: `wss://p0gyqwtakl.execute-api.ap-northeast-1.amazonaws.com/dev`,
  twilioUrlDetail:
    'https://console.twilio.com/us1/billing/manage-billing/billing-overview?frameUrl=%2Fconsole%2Fbilling%3Fx-target-region%3Dus1',
  awsUrlDetail: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
